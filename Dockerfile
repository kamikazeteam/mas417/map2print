# Step 1
FROM node:16.13.0-alpine as build-step

WORKDIR /map2print
COPY map2print_ui/ ./map2print_ui/

WORKDIR /map2print/map2print_ui
RUN npm install
RUN npm run build

# Step 2
FROM python:3.9
WORKDIR /map2print
COPY map2print_python/ ./map2print_python/
COPY --from=build-step /map2print/map2print_ui/build ./build

WORKDIR /map2print/map2print_python
RUN pip install -r requirements.txt

WORKDIR /map2print
COPY map2print.py requirements.txt ./
RUN pip install -r requirements.txt

EXPOSE 5000
CMD ["python", "map2print.py"]