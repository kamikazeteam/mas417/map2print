# map2print

Main repository for the Map2Print app, developed during the semester project of MAS417 at UIA, fall 2021.

## What is map2print

Map2print is an application that generates a 3D-printable stl-file from a map selection. 

## Installation
1. Clone repo with submodules.
    ```
    $ git clone --recurse-submodules https://gitlab.com/kamikazeteam/mas417/map2print.git
    ``` 

## Usage

1. Make sure that docker is installed by running the following command.
    **Linux:**
    ```
    $ docker -v
    ```     
    If not installed: https://docs.docker.com/get-docker/

2. Build the docker image by running in root folder.
    ```
    $ docker build -t map2print:latest .
    ```
    
3. Run docker container.
    ```
    $ docker run --rm -it -p 5000:5000 map2print:latest
    ```



## Authors
Tarjei, Martin and Martin.





