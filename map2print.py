from flask import Flask, send_from_directory, render_template, abort
from map2print_python.stl_creator.stl_creator import StlCreator
from map2print_python.fetch_terrain_data.fetch_terrain_data import FetchTerrainData
from flask_socketio import SocketIO, emit

app = Flask(__name__, static_folder="./build/static", template_folder="./build")
app.secret_key = "secret key"

# path to where the generated STL file is stored
app.config["STL_FOLDER"] = "/map2print/"
socketio = SocketIO(app)


'''
    #### Main controller class ####
'''
class Map2Print():
    # Constructor
    def __init__(self):
        self.__quality = 20

    def fetch_data(self, bbox, height_scale):
        # Fetch terrrain data
        fetch_obj = FetchTerrainData(debug=False)
        return fetch_obj.fetch_terrain_data(bbox, height_scale)

    def generate(self, data):
        # Generate STL file       
        stl_obj = StlCreator(data, self.__quality)
        stl_obj.run()


'''
    #### Flask app routes ####
'''
@app.route("/")
def index():
    return render_template('index.html')

@app.route("/get-stl")
def get_file():
    try:
        print("downloading")
        return send_from_directory(app.config["STL_FOLDER"], "mesh.stl", as_attachment=True)
    except FileNotFoundError:
        abort(404)


'''
    #### WebSocket events ####
'''
@socketio.on('generate')
def generate(data):
    
    bbox = data[:4]
    height_scale = data[4]

    emit('gen_msg', "Fetching elevation data from kartverket...")
    print("Fetching elevation data from kartverket...")

    map_data = m2p_controller.fetch_data(bbox, height_scale)

    emit('gen_msg', "Creating mesh...")
    print("Creating mesh...")

    m2p_controller.generate(map_data)
    
    print("STL ready")
    emit('download_stl', "")

@socketio.on('connect')
def test_connect():
    print("Client connected")

@socketio.on('disconnect')
def test_disconnect():
    print("Client disconnected")


if __name__ == "__main__":

    m2p_controller = Map2Print()

    socketio.run(app, host='0.0.0.0', port=5000, debug=True)